package ru.ckateptb.chat;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.command.Commands;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.dependency.DependsOn;
import ru.ckateptb.chat.command.ChatCommand;
import ru.ckateptb.chat.config.ConfigManager;
import ru.ckateptb.chat.listener.EventListener;
import ru.ckateptb.chat.listener.MsgListener;
import ru.ckateptb.chat.listener.ReplyListener;
import ru.ckateptb.chat.utils.Utils;

@org.bukkit.plugin.java.annotation.plugin.Plugin(name = "Chat", version = "1.1")
@DependsOn({@Dependency("Vault"), @Dependency("Essentials")})
@Commands({@Command(name = "chat"), @Command(name = "msg", aliases = {"w", "m", "t", "pm", "emsg", "epm", "tell", "etell", "whisper", "ewhisper"}), @Command(name = "r", aliases = {"er", "reply", "ereply"})})
public class LiteChat extends JavaPlugin {
    public Chat chat = null;
    public Plugin ess = null;
    public Utils u;

    @Override
    public void onEnable() {
        new ConfigManager(this);
        u = new Utils(this);
        new ChatCommand();
        PluginManager pm = getServer().getPluginManager();
        if (setupChat()) {
            getCommand("msg").setExecutor(new MsgListener(this));
            getCommand("r").setExecutor(new ReplyListener());
            pm.registerEvents(new EventListener(this), this);
        } else {
            pm.disablePlugin(this);
            return;
        }
        ess = pm.getPlugin("Essentials");
        if (ess == null)
            pm.disablePlugin(this);
    }

    @Override
    public void onDisable() {
        chat = null;
        ess = null;
        this.u.removeAll();
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        return chat != null;
    }
}