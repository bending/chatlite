package ru.ckateptb.chat.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.ckateptb.chat.config.ConfigManager;

public class ChatCommand implements CommandExecutor {

    public ChatCommand() {
        Bukkit.getPluginCommand("chat").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (commandSender.hasPermission("chat.reload")) {
            ConfigManager.reloadConfig(commandSender);
            return true;
        }
        return false;
    }
}
