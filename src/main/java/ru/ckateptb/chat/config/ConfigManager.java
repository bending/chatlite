package ru.ckateptb.chat.config;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

public class ConfigManager {

    private static Plugin plugin;

    public ConfigManager(Plugin pl) {
        super();
        plugin = pl;
        this.load();
    }

    private static FileConfiguration getConfig() {
        return plugin.getConfig();
    }

    public static void reloadConfig(CommandSender commandSender) {
        plugin.reloadConfig();
        commandSender.sendMessage(getConfig().getString("reloadmsg"));
    }

    private void load() {
        final FileConfiguration config = getConfig();
        config.addDefault("msgformat", "&6[&c%s &6-> &c%s&6]&r ");
        config.addDefault("msgme", "Я");
        config.addDefault("msgerr", "&6Ошибка: &4Игрок не найден.");
        config.addDefault("format", "&r{prefix}{player} &2➦&r{suffix} {message}");
        config.addDefault("reloadmsg", "Chat перезагружен.");
        config.addDefault("msgignore", "&6Ошибка: &4Игрок вас игнорирует.");
        config.options().copyDefaults(true);
        plugin.saveConfig();
    }
}
