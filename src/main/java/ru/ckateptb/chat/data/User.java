package ru.ckateptb.chat.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class User {
    private String reply;

    public Player getReply() {
        if (this.reply == null) {
            return null;
        }
        return Bukkit.getPlayerExact(this.reply);
    }

    public void setReply(Player p) {
        this.reply = p.getName();
    }
}