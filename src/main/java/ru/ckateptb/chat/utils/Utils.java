package ru.ckateptb.chat.utils;

import net.ess3.api.IEssentials;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.ckateptb.chat.LiteChat;
import ru.ckateptb.chat.data.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Utils {
    private LiteChat plugin;
    private Map<String, User> user = new ConcurrentHashMap<>();

    public Utils(LiteChat plugin) {
        this.plugin = plugin;
    }

    public static List<String> s(String st, int spl) {
        String[] arrWords = st.split(" ");
        ArrayList<String> list = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        int index = 0;
        int length = arrWords.length;
        while (index != length) {
            if (sb.length() + arrWords[index].length() <= spl) {
                sb.append(arrWords[index]).append(" ");
                index++;
            } else {
                list.add(sb.toString());
                sb.setLength(0);
            }
        }
        if (sb.length() > 0) {
            list.add(sb.toString());
        }
        return list;
    }

    public void setUser(Player p) {
        user.put(p.getName().toLowerCase(), new User());
    }

    public User getUser(Player p) {
        User us = user.get(p.getName().toLowerCase());
        if (us == null) {
            setUser(p);
            us = user.get(p.getName().toLowerCase());
        }
        return us;
    }

    public void removeUser(Player p) {
        user.remove(p.getName().toLowerCase());
    }

    public void removeAll() {
        user.clear();
    }

    public boolean
    e(String cmd, String... s) {
        for (String c : s) {
            if (cmd.equalsIgnoreCase(c)) {
                return true;
            }
        }
        for (String c : s) {
            if (cmd.equalsIgnoreCase("/essentials:" + c.substring(1))) {
                return true;
            }
        }
        for (String c : s) {
            if (cmd.equalsIgnoreCase("/" + this.plugin.getName().toLowerCase() + ":" + c.substring(1))) {
                return true;
            }
        }
        return false;
    }

    public String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public void msgsp(String message, String name) {
        if (this.plugin.ess != null) {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                if (ps.getName().equalsIgnoreCase(name)) {
                    continue;
                }
                boolean sp = false;
                if (this.plugin.ess != null) {
                    IEssentials ess = (IEssentials) this.plugin.ess;
                    sp = ess.getUser(ps).isSocialSpyEnabled();
                }
                if (sp) {
                    ps.sendMessage(name + ": " + message);
                }
            }
        }
    }

    public String replace(Player p) {
        String world = p.getWorld().getName();
        String[] group = this.plugin.chat.getPlayerGroups(p);
        String prefix = this.plugin.chat.getPlayerPrefix(p);
        String name = "{&}" + p.getName() + "{&}";
        String suffix = this.plugin.chat.getPlayerSuffix(p);
        return replace(getFormat(group[0], "format"), p, world, group[0], prefix, name, suffix);
    }

    public void send(String message, Player p, String txt) {
        if (p.hasPermission("chat.color")) {
            txt = color(txt);
        }
        if (txt.startsWith("!") && txt.length() > 1)
            txt = txt.substring(1);
        message = replace(message, txt);
        String name = "{&}" + p.getName() + "{&}";
        BaseComponent[] bmessages = generade(message, name);
        this.plugin.getServer().getConsoleSender().sendMessage(message.replace("{&}", ""));
        for (Player ps : Bukkit.getOnlinePlayers()) {
            if (this.plugin.ess != null) {
                IEssentials ess = (IEssentials) this.plugin.ess;
                boolean sp = ess.getUser(ps).isSocialSpyEnabled();
                boolean ignore = ess.getUser(ps).isIgnoredPlayer(ess.getUser(p));
                if (ignore && !sp) {
                    continue;
                }
            }
            ps.spigot().sendMessage(bmessages);
        }
    }

    public void sendcmd(String message, Player p, Player t) {
        if (this.plugin.ess != null) {
            IEssentials ess = (IEssentials) this.plugin.ess;
            boolean ignore = ess.getUser(t).isIgnoredPlayer(ess.getUser(p));
            if (ignore) {
                String msgignore = this.plugin.getConfig().getString("msgignore");
                p.sendMessage(color(msgignore));
                return;
            }
        }
        if (p.hasPermission("chat.color")) {
            message = color(message);
        }
        String rprefix = null;
        String rname = null;
        String rsuffix = null;
        if (t != null) {
            rprefix = this.plugin.chat.getPlayerPrefix(t);
            rname = "{&}" + t.getName() + "{&}";
            rsuffix = this.plugin.chat.getPlayerSuffix(t);
        }
        String format;
        String prefix = this.plugin.chat.getPlayerPrefix(p);
        String name = "{&}" + p.getName() + "{&}";
        String suffix = this.plugin.chat.getPlayerSuffix(p);
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        date.setTime(System.currentTimeMillis());
        format = color(String
                .format(this.plugin.getConfig().getString("msgformat", "[%s -> %s] "), this.plugin.getConfig().getString("msgme", "Я"), rprefix + rname + rsuffix)
                .replace("{time}", df.format(date))) + message;
        BaseComponent[] bmessages = generade(format, rname);
        p.spigot().sendMessage(bmessages);
        format = color(String
                .format(this.plugin.getConfig().getString("msgformat", "[%s -> %s] "), prefix + name + suffix, this.plugin.getConfig().getString("msgme"))
                .replace("{time}", df.format(date))) + message;
        bmessages = generade(format, name);
        assert t != null;
        t.spigot().sendMessage(bmessages);
    }

    private BaseComponent[] generade(String format, String name) {
        BaseComponent[] bmessages = TextComponent.fromLegacyText(format);
        for (int i = 0; i < bmessages.length; i++) {
            String s = bmessages[i].toLegacyText();
            if (s.contains(name)) {
                TextComponent tmp = new TextComponent();
                BaseComponent bs1 = TextComponent.fromLegacyText(s.substring(0, s.indexOf(name)))[0];
                copyFormatting(bmessages[i], bs1);
                tmp.addExtra(bs1);
                BaseComponent bs2 = TextComponent.fromLegacyText(name.replace("{&}", ""))[0];
                copyFormatting(bmessages[i], bs2);
                tmp.addExtra(bs2);
                BaseComponent[] hname = TextComponent.fromLegacyText(name.replace("{&}", ""));
                HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hname);
                bs2.setHoverEvent(hoverEvent);
                String cname = "/m " + name.replace("{&}", "") + " ";
                ClickEvent clickEvent = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, cname);
                bs2.setClickEvent(clickEvent);
                BaseComponent bs3 = TextComponent.fromLegacyText(s.substring(s.indexOf(name) + name.length()))[0];
                copyFormatting(bmessages[i], bs3);
                tmp.addExtra(bs3);
                bmessages[i] = tmp;
                break;
            }
        }
        return bmessages;
    }

    private String getFormat(String group, String format) {
        String def = this.plugin.getConfig().getString(format, "{prefix}{player}{suffix} {message}");
        return this.plugin.getConfig().getString(group + "." + format, def);
    }

    private String replace(String format, Player p, String world, String group, String prefix, String name,
                           String suffix) {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        date.setTime(System.currentTimeMillis());
        String t = df.format(date);
        format = color(format.replace("{time}", t).replace("{world}", world).replace("{group}", group)
                .replace("{prefix}", prefix).replace("{player}", name).replace("{suffix}", suffix));
        return format;
    }

    private String replace(String format, String message) {
        format = format.replace("{message}", message);
        return format;
    }

    private void copyFormatting(BaseComponent component, BaseComponent now) {
        now.setColor(component.getColorRaw());
        now.setBold(component.isBoldRaw());
        now.setItalic(component.isItalicRaw());
        now.setUnderlined(component.isUnderlinedRaw());
        now.setStrikethrough(component.isStrikethroughRaw());
        now.setObfuscated(component.isObfuscatedRaw());
        now.setInsertion(component.getInsertion());
        now.setClickEvent(component.getClickEvent());
        now.setHoverEvent(component.getHoverEvent());
        if (component.getExtra() != null) {
            for (BaseComponent extra : component.getExtra()) {
                now.addExtra(extra.duplicate());
            }
        }
    }
}